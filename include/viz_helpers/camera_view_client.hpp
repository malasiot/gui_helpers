#ifndef __CAMERA_VIEWER_SERVER_H__
#define __CAMERA_VIEWER_SERVER_H__

#include <ros/ros.h>

#include <thread>
#include <functional>
#include <atomic>
#include <mutex>

#include <viz_helpers/CameraViewFeedback.h>
#include <ros/callback_queue.h>

namespace viz_helpers {

class CameraViewClient
{
public:

    CameraViewClient( const std::string &topic_ns = "");
    ~CameraViewClient();

    void sendMessageToCameraView(const std::string &msg) ;

    typedef std::function<void (unsigned int, unsigned int)> OnMouseClickedCallback ;
    typedef std::function<void (const std::string &)> OnKeyPressedCallback ;

    void setOnMouseClickedCallback(OnMouseClickedCallback cb) {
        mouse_clicked_ = cb ;
    }

    void setOnKeyPressedCallback(OnKeyPressedCallback cb) {
        key_pressed_ = cb ;
    }

private:

    void spinThread();

    void processFeedback( const viz_helpers::CameraViewFeedbackConstPtr& feedback );

    // topic namespace to use
    std::string topic_ns_;

    std::recursive_mutex mutex_;
    std::unique_ptr<std::thread> spin_thread_;
    ros::NodeHandle node_handle_;
    ros::CallbackQueue callback_queue_;
    std::atomic<bool> need_to_terminate_{false} ;

    ros::Publisher update_pub_, message_pub_;
    ros::Subscriber feedback_sub_;

    std::string server_id_;
    OnMouseClickedCallback mouse_clicked_ = nullptr ;
    OnKeyPressedCallback key_pressed_ = nullptr ;


};

}
#endif
