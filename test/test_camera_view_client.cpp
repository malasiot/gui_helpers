#include <viz_helpers/camera_view_client.hpp>

using namespace viz_helpers ;

using namespace std ;

int main(int argc, char *argv[]) {

    ros::init(argc, argv, "test_camera_view_client") ;

    CameraViewClient client ;
    std::atomic<bool> finished{false} ;
    uint counter = 0 ;

    client.setOnKeyPressedCallback([&] (const string &key ) {
        cout << "pressed key: " << key << endl ;
        client.sendMessageToCameraView("finished");
        if ( key == "Q" ) finished = true ;
    }) ;

    client.setOnMouseClickedCallback([&] (uint x, uint y) {
        cout << "mouse clicked: " << x << ' ' << y << endl ;
        stringstream msg ;
        msg << "point " << counter++ << " (" << x << ',' << y << ')' ;
        client.sendMessageToCameraView(msg.str());
    }) ;

    while ( !finished ) ;

}


