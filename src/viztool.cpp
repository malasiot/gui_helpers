#include <osgDB/ReadFile>
#include <osgUtil/Optimizer>
#include <osg/CoordinateSystemNode>

#include <osg/Switch>
#include <osgText/Text>

#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>

#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/DriveManipulator>
#include <osgGA/KeySwitchMatrixManipulator>
#include <osgGA/StateSetManipulator>
#include <osgGA/AnimationPathManipulator>
#include <osgGA/TerrainManipulator>
#include <osgGA/SphericalManipulator>

#include <iostream>

#include "RobotScene.h"
#include "PointCloudOsg.h"
#include <sensor_msgs/PointCloud2.h>

using namespace std ;

void cloud_cb (const string &topic, osg::Group *scene, const sensor_msgs::PointCloud2ConstPtr& input)
{
    cout << "ok here" << endl;

}

int main(int argc, char** argv)
{

    ros::init(argc, argv, "viztool") ;
    ros::NodeHandle nh ;

    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;


    // use an ArgumentParser object to manage the program arguments.
    osg::ArgumentParser arguments(&argc,argv);

    arguments.getApplicationUsage()->setApplicationName(arguments.getApplicationName());
    arguments.getApplicationUsage()->setDescription(arguments.getApplicationName()+" is the standard OpenSceneGraph example which loads and visualises 3d models.");
    arguments.getApplicationUsage()->setCommandLineUsage(arguments.getApplicationName()+" [options] filename ...");
    arguments.getApplicationUsage()->addCommandLineOption("--no-robot","Do not load the robot model");
    arguments.getApplicationUsage()->addCommandLineOption("--cloud <topic>","listen to point cloud topic");
    arguments.getApplicationUsage()->addCommandLineOption("--image <filename>","Load an image and render it on a quad");
    arguments.getApplicationUsage()->addCommandLineOption("--dem <filename>","Load an image/DEM and render it on a HeightField");
    arguments.getApplicationUsage()->addCommandLineOption("--login <url> <username> <password>","Provide authentication information for http file access.");

    osgViewer::Viewer viewer(arguments);

    unsigned int helpType = 0;
    if ((helpType = arguments.readHelpType()))
    {
        arguments.getApplicationUsage()->write(std::cout, helpType);
        return 1;
    }

    // report any errors if they have occurred when parsing the program arguments.
    if (arguments.errors())
    {
        arguments.writeErrorMessages(std::cout);
        return 1;
    }

    osg::ApplicationUsage *usage = new osg::ApplicationUsage() ;

    osg::ref_ptr<osg::Group> root(new osg::Group) ;

    osg::ref_ptr<OsgRobotScene> robot ;

    string topic ;
    ros::V_Subscriber subs ;

    while ( arguments.read( "--cloud", topic ) )
    {
        ros::Subscriber sub = nh.subscribe<sensor_msgs::PointCloud2> (topic, 1, boost::bind(&cloud_cb, boost::cref(topic), root.get(), _1));
        subs.push_back(sub) ;
    }

    if ( arguments.read("--no-robot") )
    {
        root->addChild(new osg::Group) ;
    }
    else
    {
        robot = osg::ref_ptr<OsgRobotScene>(new OsgRobotScene());

        root->addChild(robot.get()) ;

        KeyboardEventHandler *keyHandler = new KeyboardEventHandler(robot.get()) ;
        keyHandler->getUsage(*usage) ;

        viewer.addEventHandler(keyHandler);

        PickEventHandler *pickHandler = new  PickEventHandler() ;

        viewer.addEventHandler(pickHandler) ;
    }

    viewer.setCameraManipulator( new osgGA::TrackballManipulator() );


    // add the state manipulator
    viewer.addEventHandler( new osgGA::StateSetManipulator(viewer.getCamera()->getOrCreateStateSet()) );

    // add the thread model handler
    viewer.addEventHandler(new osgViewer::ThreadingHandler);

    // add the window size toggle handler
    viewer.addEventHandler(new osgViewer::WindowSizeHandler);

    // add the stats handler
    viewer.addEventHandler(new osgViewer::StatsHandler);

    // add the help handler
    viewer.addEventHandler(new osgViewer::HelpHandler(arguments.getApplicationUsage()));

    // add the screen capture handler
    viewer.addEventHandler(new osgViewer::ScreenCaptureHandler);

    // load the data

    osgDB::Registry::instance()->addReaderWriter(new PointCloudReader);

    osg::ref_ptr<osg::Node> loadedModel = osgDB::readNodeFiles(arguments);

    if ( loadedModel )
        root->addChild(loadedModel.get()) ;

    // any option left unread are converted into errors to write out later.
    arguments.reportRemainingOptionsAsUnrecognized();

    // report any errors if they have occurred when parsing the program arguments.
    if (arguments.errors())
    {
        arguments.writeErrorMessages(std::cout);
        return 1;
    }

    // optimize the scene graph, remove redundant nodes and state etc.
    osgUtil::Optimizer optimizer;
    optimizer.optimize(root.get());

    viewer.setSceneData( root.get() );

    viewer.realize();

    while (!viewer.done()) {
        if ( robot ) robot->updateJointState();
        viewer.frame();
    }

    return viewer.run();

}
