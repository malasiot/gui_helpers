#include "launcher.h"

#include <QDebug>
#include <QApplication>
#include <QTabWidget>
#include <QXmlStreamReader>
#include <QFile>
#include <QMessageBox>

#include <iostream>

/////////////////////////////////////////////////////////////////////////////
/// \brief LaunchManager::LaunchManager
/// \param cmd
///
///

AnsiEscapeCodeHandler::AnsiEscapeCodeHandler() :
    m_previousFormatClosed(true)
{
}

static QColor ansiColor(uint code)
{

    const int red   = code & 1 ? 170 : 0;
    const int green = code & 2 ? 170 : 0;
    const int blue  = code & 4 ? 170 : 0;
    return QColor(red, green, blue);
}

QList<FormattedText> AnsiEscapeCodeHandler::parseText(const FormattedText &input)
{
    enum AnsiEscapeCodes {
        ResetFormat            =  0,
        BoldText               =  1,
        TextColorStart         = 30,
        TextColorEnd           = 37,
        RgbTextColor           = 38,
        DefaultTextColor       = 39,
        BackgroundColorStart   = 40,
        BackgroundColorEnd     = 47,
        RgbBackgroundColor     = 48,
        DefaultBackgroundColor = 49
    };

    QList<FormattedText> outputData;

    QTextCharFormat charFormat = m_previousFormatClosed ? input.format : m_previousFormat;

    const QString escape = QLatin1String("\x1b[");
    const int escapePos = input.text.indexOf(escape);
    if (escapePos < 0) {
        outputData << FormattedText(input.text, charFormat);
        return outputData;
    } else if (escapePos != 0) {
        outputData << FormattedText(input.text.left(escapePos), charFormat);
    }

    const QChar semicolon       = QLatin1Char(';');
    const QChar colorTerminator = QLatin1Char('m');
    // strippedText always starts with "\e["
    QString strippedText = input.text.mid(escapePos);
    while (!strippedText.isEmpty()) {
        while (strippedText.startsWith(escape)) {
            strippedText.remove(0, 2);

            // get the number
            QString strNumber;
            QStringList numbers;
            while (strippedText.at(0).isDigit() || strippedText.at(0) == semicolon) {
                if (strippedText.at(0).isDigit()) {
                    strNumber += strippedText.at(0);
                } else {
                    numbers << strNumber;
                    strNumber.clear();
                }
                strippedText.remove(0, 1);
            }
            if (!strNumber.isEmpty())
                numbers << strNumber;

            // remove terminating char
            if (!strippedText.startsWith(colorTerminator)) {
                strippedText.remove(0, 1);
                continue;
            }
            strippedText.remove(0, 1);

            if (numbers.isEmpty()) {
                charFormat = input.format;
                endFormatScope();
            }

            for (int i = 0; i < numbers.size(); ++i) {
                const int code = numbers.at(i).toInt();

                if (code >= TextColorStart && code <= TextColorEnd) {
                    charFormat.setForeground(ansiColor(code - TextColorStart));
                    setFormatScope(charFormat);
                } else if (code >= BackgroundColorStart && code <= BackgroundColorEnd) {
                    charFormat.setBackground(ansiColor(code - BackgroundColorStart));
                    setFormatScope(charFormat);
                } else {
                    switch (code) {
                    case ResetFormat:
                        charFormat = input.format;
                        endFormatScope();
                        break;
                    case BoldText:
                        charFormat.setFontWeight(QFont::Bold);
                        setFormatScope(charFormat);
                        break;
                    case DefaultTextColor:
                        charFormat.setForeground(input.format.foreground());
                        setFormatScope(charFormat);
                        break;
                    case DefaultBackgroundColor:
                        charFormat.setBackground(input.format.background());
                        setFormatScope(charFormat);
                        break;
                    case RgbTextColor:
                    case RgbBackgroundColor:
                        if (++i >= numbers.size())
                            break;
                        if (numbers.at(i).toInt() == 2) {
                            // RGB set with format: 38;2;<r>;<g>;<b>
                            if ((i + 3) < numbers.size()) {
                                (code == RgbTextColor) ?
                                      charFormat.setForeground(QColor(numbers.at(i + 1).toInt(),
                                                                      numbers.at(i + 2).toInt(),
                                                                      numbers.at(i + 3).toInt())) :
                                      charFormat.setBackground(QColor(numbers.at(i + 1).toInt(),
                                                                      numbers.at(i + 2).toInt(),
                                                                      numbers.at(i + 3).toInt()));
                                setFormatScope(charFormat);
                            }
                            i += 3;
                        } else if (numbers.at(i).toInt() == 5) {
                            // rgb set with format: 38;5;<i>
                            // unsupported because of unclear documentation, so we just skip <i>
                            ++i;
                        }
                        break;
                    default:
                        break;
                    }
                }
            }
        }

        if (strippedText.isEmpty())
            break;
        int index = strippedText.indexOf(escape);
        if (index > 0) {
            outputData << FormattedText(strippedText.left(index), charFormat);
            strippedText.remove(0, index);
        } else if (index == -1) {
            outputData << FormattedText(strippedText, charFormat);
            break;
        }
    }
    return outputData;
}

void AnsiEscapeCodeHandler::endFormatScope()
{
    m_previousFormatClosed = true;
}

void AnsiEscapeCodeHandler::setFormatScope(const QTextCharFormat &charFormat)
{
    m_previousFormat = charFormat;
    m_previousFormatClosed = false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////

LaunchManager::LaunchManager()
{
    // Layout
  /*
    connect( quitButton, SIGNAL(clicked()),
            this, SLOT(quit()) );
*/

    layout = new QVBoxLayout(this) ;
    buttons = new QHBoxLayout(this) ;


    layout->addLayout(buttons) ;

    tabs = new QTabWidget(this) ;
    layout->addWidget(tabs) ;

    resize( 700, 500 );
/*
    // QProcess related code
    proc = new QProcess( this );

    // Set up the command and arguments.
    // On the command line you would do:
    //   uic -tr i18n "small_dialog.ui"

    connect( proc, SIGNAL(readyReadStandardOutput()),
            this, SLOT(readFromStdout()) );
    connect( proc, SIGNAL(finished(int)),
            this, SLOT(scrollToTop()) );

    proc->start(cmd) ;
*/
}

void LaunchManager::addCommand(const QString &cmd, const QString &label)
{
    // QProcess related code
    QProcess *proc_ = new QProcess( this );
    proc_->setProperty("name", label) ;

    proc_->setProcessChannelMode(QProcess::MergedChannels);

    proc.insert(label, proc_) ;
    cmds.insert(label, cmd) ;

    QPushButton *button = new QPushButton(QIcon(":/images/start-icon.png"), label, this) ;

    QTextBrowser *console = new QTextBrowser( this );

    button->setProperty("state", "stopped") ;

    connect(button, SIGNAL(clicked()), this, SLOT(onClicked())) ;

    output.insert(label, console) ;

    tabs->addTab(console, label) ;
    buttons->addWidget(button) ;

    connect( proc_, SIGNAL(readyReadStandardOutput()),
            this, SLOT(readFromStdout()) );
    connect( proc_, SIGNAL(finished(int)),
            this, SLOT(scrollToTop()) );



}

void LaunchManager::readFromStdout()
{
    QProcess *proc_ = (QProcess *)sender() ;
    QString label = proc_->property("name").toString() ;

    AnsiEscapeCodeHandler handler ;
    QList<FormattedText> chunks = handler.parseText(FormattedText(proc_->readAll())) ;

    QTextCursor cursor(output[label]->document());

    cursor.movePosition(QTextCursor::End);

    Q_FOREACH (const FormattedText &output, chunks) {
        cursor.insertText(output.text, output.format);
   }

   QTextBrowser *t = output[label] ;
   t->setTextCursor(cursor) ;

}

void LaunchManager::scrollToTop()
{

}

void LaunchManager::onClicked()
{
    QPushButton *button = (QPushButton *)sender() ;

    QString label = button->text() ;

    if ( button->property("state").toString() == "stopped" )
    {
        output[label]->clear() ;
        tabs->setCurrentWidget(output[label]);

        proc[label]->start(cmds[label]) ;
        button->setIcon(QIcon(":/images/stop-red-icon.png")) ;
        button->setProperty("state", "running") ;

    }
    else
    {
        proc[label]->terminate() ;
    //    proc[label]->kill() ;
        button->setIcon(QIcon(":/images/start-icon.png")) ;
        button->setProperty("state", "stopped") ;
    }

}

void LaunchManager::parseXML(const QString &fileName) {
    /* We'll parse the example.xml */
    QFile* file = new QFile(fileName);

    /* If we can't open it, let's show an error message. */

    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::critical(this,
                              "LaunchManager::parseXML",
                              QString("Couldn't open ") + fileName,
                              QMessageBox::Ok);
        return;
    }
    /* QXmlStreamReader takes any QIODevice. */
    QXmlStreamReader xml(file);
    QVector< QMap<QString,QString> > commands;
    /* We'll parse the XML until we reach end of it.*/
    while(!xml.atEnd() && !xml.hasError()) {
        /* Read next element.*/
        QXmlStreamReader::TokenType token = xml.readNext();
        /* If token is just StartDocument, we'll go to next.*/
        if(token == QXmlStreamReader::StartDocument) {
            continue;
        }
        /* If token is StartElement, we'll see if we can read it.*/
        if(token == QXmlStreamReader::StartElement) {
            /* If it's named persons, we'll go to the next.*/
            if(xml.name() == "commands") {
                continue;
            }
            /* If it's named person, we'll dig the information from there.*/
            if(xml.name() == "launch") {
                commands.append(this->parseLaunch(xml));
            }
        }
    }
    /* Error handling. */
    if(xml.hasError()) {
        QMessageBox::critical(this,
                              "LaunchManager::parseXML",
                              xml.errorString(),
                              QMessageBox::Ok);
    }
    /* Removes any device() or data from the reader
     * and resets its internal state to the initial state. */
    xml.clear();

    for(int i=0 ; i<commands.size() ; i++ )
    {
        const QMap<QString, QString> &launch = commands[i] ;
        addCommand(launch["command"], launch["id"]) ;
    }
}

QMap<QString, QString> LaunchManager::parseLaunch(QXmlStreamReader& xml) {
    QMap<QString, QString> launch;
    /* Let's check that we're really getting a person. */
    if(xml.tokenType() != QXmlStreamReader::StartElement &&
            xml.name() == "launch") {
        return launch;
    }
    /* Let's get the attributes for person */
    QXmlStreamAttributes attributes = xml.attributes();
    /* Let's check that person has id attribute. */
    if(attributes.hasAttribute("id")) {
        /* We'll add it to the map. */
        launch["id"] = attributes.value("id").toString();
    }
    /* Next element... */
    xml.readNext();
    /*
     * We're going to loop over the things because the order might change.
     * We'll continue the loop until we hit an EndElement named person.
     */
    while(!(xml.tokenType() == QXmlStreamReader::EndElement &&
            xml.name() == "launch")) {
        if(xml.tokenType() == QXmlStreamReader::StartElement) {
            /* We've found first name. */
            if(xml.name() == "command") {
                this->addElementDataToMap(xml, launch);
            }

        }
        /* ...and next... */
        xml.readNext();
    }
    return launch;
}

void LaunchManager::addElementDataToMap(QXmlStreamReader& xml,
                                      QMap<QString, QString>& map) const {
    /* We need a start element, like <foo> */
    if(xml.tokenType() != QXmlStreamReader::StartElement) {
        return;
    }
    /* Let's read the name... */
    QString elementName = xml.name().toString();
    /* ...go to the next. */
    xml.readNext();
    /*
     * This elements needs to contain Characters so we know it's
     * actually data, if it's not we'll leave.
     */
    if(xml.tokenType() != QXmlStreamReader::Characters) {
        return;
    }
    /* Now we can add it to the map.*/
    map.insert(elementName, xml.text().toString());
}

using namespace std ;

int main(int argc, char *argv[])
{
    QApplication app(argc, argv) ;

    if ( argc < 2 )
    {
        cout << "Usage: launcher <xml launch specification>" << endl ;
        exit(1) ;
    }

    LaunchManager manager ;
    manager.parseXML(argv[1]) ;

    manager.show() ;

    app.exec() ;

}
