#ifndef _CAMERA_VIEW_H_
#define _CAMERA_VIEW_H_

#include "camera_widget.hpp"

#include <viz_helpers/CameraViewFeedback.h>

#include <QScrollArea>
#include <QGridLayout>
#include <QComboBox>

#include <image_transport/subscriber.h>

#include <ros/callback_queue.h>


namespace viz_helpers {

class QCameraView: public QScrollArea
{
	Q_OBJECT

public:

    QCameraView(QWidget *parent, ros::NodeHandle handle, const std::vector<std::string> &imageTopics, const std::string &topic_ns = std::string()) ;
    ~QCameraView() ;

     void cleanup() ;

public Q_SLOTS:

    void updateFrame(const QImage &im) ;
    void onTopicChanged(int index) ;

 Q_SIGNALS:

    void frameChanged(const QImage &im) ;
protected:

	bool eventFilter(QObject *o, QEvent *e) ;

    void setTopic(const std::string &topic) ;

private:
    QCameraWidget *imageWidget ;
    QComboBox *sourceCombo ;
    QLabel *messageWidget ;

	QScrollArea *container ;
    QVBoxLayout *vbl ;

    image_transport::Subscriber subscriber_;
    ros::Publisher feedback_pub_ ;
    ros::Subscriber message_sub_ ;

    QMutex image_ ;
    ros::NodeHandle handle ;
    std::vector<std::string> topics ;

    boost::mutex connect_mutex_ ;
    bool needMouseFeedback, needKeyFeedback ;

    virtual void callbackImage(const sensor_msgs::Image::ConstPtr& msg);

    void feedbackCb(const ros::SingleSubscriberPublisher &pub) ;
    void messageCb(const viz_helpers::CameraViewFeedbackConstPtr &msg) ;
} ;


}

#endif
