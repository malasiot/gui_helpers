#include <ros/ros.h>
#include <QtGui>
#include <QApplication>
#include <QMainWindow>

#include <cvx/util/misc/arg_parser.hpp>
#include <cvx/util/misc/strings.hpp>

#include "camera_view.hpp"

using namespace std ;
using namespace viz_helpers ;
using namespace cvx::util ;

int main(int argc, char *argv[])
{
	QApplication app(argc, argv) ;

    ros::init(argc, argv, "camera_viewer") ;

    ArgumentParser args ;

    bool print_help = false ;
    string topics_str, title_str ;

    args.addOption("-h|--help", print_help, true).setMaxArgs(0).setDescription("print this help message") ;
    args.addOption("--topics", topics_str).setDescription("colon separated list of of topics").setName("<topics>") ;
    args.addOption("--title", title_str).setDescription("window title").setName("<title>") ;

    if ( !args.parse(argc, (const char **)argv) || print_help ) {
        cout << "Usage: camera_viewer [options]" << endl ;
        cout << "Options:" << endl ;
        args.printOptions(std::cout) ;
        exit(1) ;
    }

    ros::NodeHandle nh("~") ;

    QMainWindow *mwin = new QMainWindow() ;

    nh.getParam("topics", topics_str) ;

    if ( topics_str.empty() )
    {
        ROS_ERROR("No input topic specified") ;
        exit(1) ;
    }

    std::vector<string> topics = split(topics_str, ";:");

    viz_helpers::QCameraView *cam = new viz_helpers::QCameraView(mwin, nh, topics) ;
    mwin->setCentralWidget(cam);

    nh.getParam("title", title_str) ;

    if ( !title_str.empty() )
        mwin->setWindowTitle(title_str.c_str());


    mwin->show() ;

    ros::AsyncSpinner spinner(1) ;
    spinner.start() ;

	app.exec() ;
	

}
