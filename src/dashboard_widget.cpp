#include "dashboard_widget.hpp"

#include <robot_helpers/robot.hpp>

using namespace robot_helpers ;
using namespace std ;

namespace viz_helpers {

void RobotDashboard::openLeftGripper()
{
  //  ui.openLeftGripperButton->setEnabled(false);
    ros::Duration(ui.gripperDelay->value()).sleep() ;
    rb2_.openGripper() ;
    //ui.messageBox->setText("Failed to open left hand gripper.") ;
    ros::Duration(0.5).sleep() ;
    updateState() ;

}

void RobotDashboard::openRightGripper()
{
 //   ui.openRightGripperButton->setEnabled(false);
    ros::Duration(ui.gripperDelay->value()).sleep() ;
    rb1_.openGripper() ;
    //ui.messageBox->setText("Failed to open right hand gripper.") ;
    ros::Duration(0.5).sleep() ;
    updateState() ;
}


void RobotDashboard::closeLeftGripper()
{

   // ui.closeLeftGripperButton->setEnabled(false);
    ros::Duration(ui.gripperDelay->value()).sleep() ;
    rb2_.closeGripper() ;
    //ui.messageBox->setText("Failed to close left hand gripper.") ;
    ros::Duration(0.5).sleep() ;
    updateState() ;
}

void RobotDashboard::closeRightGripper()
{

    //ui.closeRightGripperButton->setEnabled(false);

    ros::Duration(ui.gripperDelay->value()).sleep() ;
    rb1_.closeGripper() ;
    //ui.messageBox->setText("Failed to close right hand gripper.") ;

    ros::Duration(0.5).sleep() ;
    updateState() ;

}

void RobotDashboard::setServoPowerOff()
{
      rb1_.setServoPowerOff();
}

void RobotDashboard::moveTorso() {

        //robot_helpers::rotateTorso(rb, ui.angle->value()*M_PI/180.0);
}



void RobotDashboard::updateState()
{
    {
        bool state = rb1_.isGripperOpen() ;
        ui.openRightGripperButton->setEnabled(!state) ;
        ui.closeRightGripperButton->setEnabled(state) ;
    }

    {
        bool state = rb2_.isGripperOpen() ;
        ui.openLeftGripperButton->setEnabled(!state) ;
        ui.closeLeftGripperButton->setEnabled(state) ;
    }

}


void RobotDashboard::moveHome()
{
    rb1_.moveHome() ;
    rb2_.moveHome() ;
}

void RobotDashboard::moveHomeArms()
{
    rb1_.moveHome() ;
    rb2_.moveHome() ;
}

} // namespace

