#ifndef __DASHBOARD_WIDGET_HPP__
#define __DASHBOARD_WIDGET_HPP__

#include "ui_dashboard_widget.h"

#include <QWidget>
#include <ros/ros.h>
#include <robot_helpers/robot.hpp>

namespace viz_helpers {

class RobotDashboard: public QWidget
{
    Q_OBJECT

public:

    RobotDashboard(ros::NodeHandle nh, QWidget *parent = 0): QWidget(parent), handle(nh)
    {
        ui.setupUi(this);
        updateState() ;

    }

private Q_SLOTS:
    void openLeftGripper() ;
    void closeLeftGripper() ;
    void openRightGripper() ;
    void closeRightGripper() ;
    void moveHome() ;
    void moveHomeArms() ;
    void setServoPowerOff() ;
    void moveTorso() ;

private:

    void updateState() ;
    Ui::DashboardWidget ui;
    ros::NodeHandle handle ;

    robot_helpers::RobotArm1 rb1_ ;
    robot_helpers::RobotArm2 rb2_ ;
 };



} // namespace

#endif
