#include <viz_helpers/camera_view_client.hpp>

using namespace std ;

namespace viz_helpers {

CameraViewClient::CameraViewClient( const std::string &topic_ns )
{

    node_handle_.setCallbackQueue( &callback_queue_ );

    std::string feedback_topic = topic_ns + "camera_viewer/feedback";
    std::string message_topic = topic_ns + "camera_viewer/message";

    feedback_sub_ = node_handle_.subscribe( feedback_topic, 100, &CameraViewClient::processFeedback, this );
    message_pub_ = node_handle_.advertise<viz_helpers::CameraViewFeedback>( message_topic, 100 );

    need_to_terminate_ = false ;
    spin_thread_.reset( new std::thread(&CameraViewClient::spinThread, this) );

}

void CameraViewClient::sendMessageToCameraView(const std::string &text)
{
    CameraViewFeedback msg ;
    msg.message = text ;
    message_pub_.publish(msg) ;
}

CameraViewClient::~CameraViewClient()
{
    if (spin_thread_.get())
    {
        need_to_terminate_ = true;
        spin_thread_->join();
    }
}


void CameraViewClient::spinThread()
{
    while ( node_handle_.ok() )
    {
        if ( need_to_terminate_ ) break;
        callback_queue_.callAvailable(ros::WallDuration(0.033f));
    }
}

void CameraViewClient::processFeedback( const CameraViewFeedbackConstPtr& feedback )
{
    std::unique_lock<std::recursive_mutex> lock( mutex_ );

    if ( feedback->feedback_type == CameraViewFeedback::MOUSE_CLICKED && mouse_clicked_ )
        mouse_clicked_(feedback->mouse_point.x, feedback->mouse_point.y) ;
    else if ( feedback->feedback_type == CameraViewFeedback::KEY_PRESSED && key_pressed_ )
        key_pressed_(feedback->key) ;
}

}
