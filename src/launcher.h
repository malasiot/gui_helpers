#ifndef __LAUNCHER_H__
#define __LAUNCHER_H__

#include <QProcess>
#include <QVBoxLayout>
#include <QTextBrowser>
#include <QPushButton>
#include <QDebug>
#include <QTabWidget>

#include <QTextCharFormat>
#include <QXmlStreamReader>

class LaunchManager : public QWidget
{
    Q_OBJECT

public:
    LaunchManager();
    ~LaunchManager() {}

    void parseXML(const QString &fileName) ;
    void addCommand(const QString &cmd, const QString &label) ;

public Q_SLOTS:
    void readFromStdout();
    void scrollToTop();
    void onClicked() ;

protected:
    QMap<QString, QString> parseLaunch(QXmlStreamReader &xml);
    void addElementDataToMap(QXmlStreamReader &xml, QMap<QString, QString> &map) const;
private:
    QMap<QString, QProcess *> proc;
    QMap<QString, QTextBrowser *>output;
    QMap<QString, QString> cmds ;
    QPushButton *quitButton;
    QVBoxLayout *layout ;
    QHBoxLayout *buttons ;
    QTabWidget *tabs ;
};

class FormattedText {

public:

    FormattedText() { }
    FormattedText(const FormattedText &other) : text(other.text), format(other.format) { }
    FormattedText(const QString &txt, const QTextCharFormat &fmt = QTextCharFormat()) :
    text(txt), format(fmt)
    { }

    QString text;
    QTextCharFormat format;
};

class AnsiEscapeCodeHandler
{
public:
    AnsiEscapeCodeHandler();
    QList<FormattedText> parseText(const FormattedText &input);
    void endFormatScope();

private:
    void setFormatScope(const QTextCharFormat &charFormat);
    bool m_previousFormatClosed;
    QTextCharFormat m_previousFormat;
};





#endif
